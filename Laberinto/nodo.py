class nodo_estado:
    def __init__(self, v, p, a, n):
        self.valor = v
        self.padre = p
        self.accion = a
        self.nivel = n
        self.distancia = None

    def get_estado(self):
        return self.valor
    
    def get_padre(self):
        return self.padre

    def get_accion(self):
        return self.accion

    def get_nivel(self):
        return self.nivel

    def __eq__(self, e):
        return self.valor == e


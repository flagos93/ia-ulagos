from laberinto import laberinto

""" if __name__ == "__main__":
    puzzle = laberinto("123H56478")
    #puzzle.algoritmo_anchura()
    #puzzle.algoritmo_profundidad()
    #puzzle.algoritmo_anchura_evalua_hijos()
    puzzle.algoritmo_profundidad_evalua_hijos() """

def leer_mapa(mapa):
    archivo = mapa
    laberinto = []
    i = 0

    with open(archivo) as en_archivo:
        print("\n")
        for linea in en_archivo:
            laberinto.append([])
            posicion = linea.split(' ')
            for p in posicion:
                laberinto[i].append(p)
            i += 1
    
    return laberinto

if __name__ == "__main__":
    coordenada = [0,0]
    finales = [[9,10]]
    lab = laberinto(coordenada, finales, leer_mapa("Info/lab.dat"))
    
    lab.busqueda()
